import {Framework, Utils} from "floose";
import Server = Framework.Server;

export declare enum ModbusObjectType {
    COIL = "coil",
    DISCRETE_INPUT = "discrete-input",
    HOLDING_REGISTER = "holding-register",
    INPUT_REGISTER = "input-register"
}

/**
 * Integrations
 */
interface NumberRange {
    from: number;
    to: number;
}
interface CoilsHandler {
    type: ModbusObjectType.COIL;
    unitId: number[];
    addresses: (NumberRange | number)[];
    read(address: number): Promise<boolean>;
    write(address: number, value: boolean): Promise<void>;
}
interface DiscreteInputsHandler {
    type: ModbusObjectType.DISCRETE_INPUT;
    unitId: number[];
    addresses: (NumberRange | number)[];
    read(address: number): Promise<boolean>;
}
interface HoldingRegistersHandler {
    type: ModbusObjectType.HOLDING_REGISTER;
    unitId: number[];
    addresses: (NumberRange | number)[];
    read(address: number): Promise<Buffer | number>;
    write(address: number, value: number): Promise<void>;
}
interface InputRegistersHandler {
    type: ModbusObjectType.INPUT_REGISTER;
    unitId: number;
    addresses: number[];
    read(address: number): Promise<Buffer | number>;
}
export declare type Handler = CoilsHandler | DiscreteInputsHandler | HoldingRegistersHandler | InputRegistersHandler;


/**
 * Server
 */
export declare interface ModbusServerConfig {
    /**
     *
     */
    host?: string;
    /**
     *
     */
    port: number;
}

export declare class ModbusServer extends Server {
    readonly configurationValidationSchema: Utils.Validation.Schema;
    readonly running: boolean;
    init(config: ModbusServerConfig): Promise<void>;
    start(): Promise<void>;
    stop(): Promise<void>;

    /**
     *
     * @param command
     */
    addHandler(command: Handler): string;

    /**
     *
     * @param identifier
     */
    removeHandler(identifier: string): void;
}